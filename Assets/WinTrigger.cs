﻿using UnityEngine;
using UnityEngine.Events;

public class WinTrigger : MonoBehaviour
{
    [SerializeField] private UnityEvent _onWin;

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (other.gameObject.CompareTag("Player"))
        {
            GameManager.Instance.Player.gameObject.SetActive(false);
            _onWin.Invoke();
            GameManager.Instance.WinEvent.Invoke();
        }
    }
}
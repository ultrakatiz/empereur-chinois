﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FollowingObject : MonoBehaviour
{
    [SerializeField] private GameObject _followedObject;
    [SerializeField] private Vector2 _offset = new Vector2(6.0f, 0.0f);

    [SerializeField] private Transform _wc;
    [SerializeField] private float _stopOffset = -6.0f;

    private MovingBackground[] _movingBackgrounds = new MovingBackground[2];

    private bool _stopped;

    private void Awake()
    {
        if (_followedObject == null)
        {
            Debug.Log(name + "'s FollowingObject's Followed Object is not set. Searching for the player...");
            _followedObject = GameObject.FindGameObjectWithTag("Player");

            if (_followedObject != null)
            {
                Debug.Log(name + "'s FollowingObject found a Player !");
                return;
            }

            Debug.LogError(name + "'s FollowingObject did not find any GameObject tagged with 'Player'."
                           + " Destroying FollowingObject.");
            Destroy(this);
        }

        if (_wc == null) _wc = FindObjectOfType<WinTrigger>()?.transform;

        _movingBackgrounds = GetComponentsInChildren<MovingBackground>();
    }

    private void Update()
    {
        if (!_stopped)
            transform.position = new Vector3(
                _followedObject.transform.position.x + _offset.x,
                transform.position.y + _offset.y,
                transform.position.z);

        if (_wc == null || transform.position.x <= _wc.position.x + _stopOffset)
            return;

        transform.position = new Vector3(
            _wc.position.x + _stopOffset,
            transform.position.y,
            transform.position.z);

        for (var i = 0; i < _movingBackgrounds.Length; i++)
        {
            if(_movingBackgrounds[i] != null)
                _movingBackgrounds[i].StopScrolling = true;
        }

        _stopped = true;
    }
}
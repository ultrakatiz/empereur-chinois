﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class PlayerStats : MonoBehaviour
{
    public UnityEvent OnDeath;
    public UnityEvent BeforeDeath;
    //Player Speed
    [SerializeField]
    private float runSpeed;
    public float RunSpeed
    {
        get { return runSpeed; }
        set { runSpeed = value; }
    }

    [SerializeField]
    private float walkSpeed;
    public float WalkSpeed
    {
        get { return walkSpeed; }
        set { walkSpeed = value; }
    }

    //IsDashing
    [SerializeField]
    private float dashSpeed;
    public float DashSpeed
    {
        get { return dashSpeed; }
        set { dashSpeed = value; }
    }

    [SerializeField]
    private float dashDuration;
    public float DashDuration
    {
        get { return dashDuration; }
        set { dashDuration = value; }
    }

    [SerializeField]
    private float dashCooldown;
    public float DashCooldown
    {
        get { return dashCooldown; }
        set { dashCooldown = value; }
    }

    //Health
    [SerializeField]
    private int maxHealthPoints;
    public int MaxHealthPoints
    {
        get { return maxHealthPoints; }
        set { maxHealthPoints = value; }
    }

    [SerializeField]
    private int currentHealthPoints;
    public int CurrentHealthPoints
    {
        get { return currentHealthPoints; }
        set { currentHealthPoints = value; }
    }

    [SerializeField]
    private int decreaseHPRate;
    public int DecreaseHPRate
    {
        get { return decreaseHPRate; }
        set { decreaseHPRate = value; }
    }

    [SerializeField]
    private float decreaseHPFrequency;
    public float DecreaseHPFrequency
    {
        get { return decreaseHPFrequency; }
        set { decreaseHPFrequency = value; }
    }

    [SerializeField]
    private int healthIncreaseWhenDash;
    public int HealthIncreaseWhenDash
    {
        get { return healthIncreaseWhenDash; }
        set { healthIncreaseWhenDash = value; }
    }

    [SerializeField] private float forcedWalkTimeOnCarpetCollision;
    public float ForcedWalkTimeOnCarpetCollision
    {
        get => forcedWalkTimeOnCarpetCollision;
        set => forcedWalkTimeOnCarpetCollision = value;
    }

    public bool isDead = false;

    private float decreaseHPTimer;


    private void Awake()
    {
        CurrentHealthPoints = MaxHealthPoints;
        decreaseHPTimer = 0f;
    }

    private void Update()
    {
        UpdateHealth();
    }

    public void UpdateHealth()
    {
        decreaseHPTimer += Time.deltaTime;
        if(decreaseHPTimer >= DecreaseHPFrequency)
        {
            decreaseHPTimer = 0f;
            CurrentHealthPoints -= DecreaseHPRate;
        }
        
        if(CheckDeath())
            BeforeDeath.Invoke();
    }

    public void IncreaseHealthDash()
    {
        CurrentHealthPoints = Mathf.Min(MaxHealthPoints, CurrentHealthPoints + healthIncreaseWhenDash);
    }

    private bool CheckDeath()
    {
        if (CurrentHealthPoints <= 0)
        {
            GameManager.Instance.ScoreIncrementPerSecond = 0;
            isDead = true;
            return true;
        }
        return false;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D))]
[RequireComponent(typeof(Controller), typeof(PlayerStats))]
public class Player : MonoBehaviour
{
    public Rigidbody2D Rigidbody { get; private set; }
    public Collider2D Collider { get; private set; }
    public PlayerStats Stats { get; private set; }
    public Controller Controller { get; private set; }
    public Animator Animator { get; private set; }

    private float _forceWalkTimer;
    private bool wasForcedWalkLastFrame;
    public UnityEvent onShameWalkStart;
    public UnityEvent onStaggered;
    private void Awake()
    {
        Rigidbody = GetComponent<Rigidbody2D>();
        if (Rigidbody == null)
        {
            Debug.LogWarning(name + " has a Player but no Rigidbody2D. Destroying the Player.");
            Destroy(this);
            return;
        }
        Rigidbody.bodyType = RigidbodyType2D.Kinematic;

        Collider = GetComponent<Collider2D>();
        if (Collider == null)
        {
            Debug.LogWarning(name + " has a Player but no Collider2D. Destroying the Player.");
            Destroy(this);
            return;
        }

        Stats = GetComponent<PlayerStats>();
        if (Stats == null)
        {
            Debug.LogWarning(name + " has a Player but no PlayerStats. Destroying the Player.");
            Destroy(this);
            return;
        }

        Controller = GetComponent<Controller>();
        if (Controller == null)
        {
            Debug.LogWarning(name + " has a Player but no Controller. Destroying the Player.");
            Destroy(this);
            return;
        }

        Animator = GetComponent<Animator>();
        if (Animator == null)
        {
            Debug.LogWarning(name + " has a Controller but no Animtor. Destroying the Controller.");
            Destroy(this);
            return;
        }
    }

    private void Start()
    {
        GameManager.Instance.ObstacleCollisionEvent.AddListener(OnObstacleCollision);
        Stats.BeforeDeath.AddListener(DeathAnimation);
    }

    private void Update()
    {
        if (_forceWalkTimer > 0.0f)
        {
            _forceWalkTimer -= Time.deltaTime;
            Controller.SetForceWalk(true);
            Animator.SetBool("ShameWalk",true);
            if(!wasForcedWalkLastFrame)
                onShameWalkStart.Invoke();
            wasForcedWalkLastFrame = true;
        }
        else
        {
            Controller.SetForceWalk(false);
            Animator.SetBool("ShameWalk", false);
            wasForcedWalkLastFrame = false;
        }

    }

    private void OnObstacleCollision(Obstacle obstacle)
    {
        if(GameManager.Instance.CurrentZone != null && GameManager.Instance.CurrentZone.type == ZoneStats.ZoneType.Crowded)
            GameManager.Instance.Score -= 2*obstacle.ScoreDamage;
        else
            GameManager.Instance.Score -= obstacle.ScoreDamage;

        Debug.Log("Player collided with " + obstacle.name);

        if (obstacle.CompareTag("staggering"))
        {
            onStaggered.Invoke();
            Animator.SetTrigger("fall");
            _forceWalkTimer = Stats.ForcedWalkTimeOnCarpetCollision;
        }
    }

    private void DeathAnimation()
    {
        Animator.SetTrigger("Death");
    }
}

﻿using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Collider2D), typeof(Rigidbody2D), typeof(PlayerStats))]
public class Controller : MonoBehaviour
{
#if UNITY_ANDROID
    [SerializeField] private float _tapTime = 0.05f;
    [SerializeField] private float _doubleTapTime = 0.2f;
    private float _timeSinceLastTouchBegan;
    private float _timeSinceLastTouchEnded;
#else
    [SerializeField] private KeyCode _walkKey = KeyCode.Mouse0;
    [SerializeField] private KeyCode _dashKey = KeyCode.Mouse1;
#endif

    public bool Stopped { get; private set; }
    private float _stopTimer;

    public bool IsWalking { get; private set; }
    public bool _wasWalkingLastFrame;
    private float _dashTimer;

    [SerializeField] private UnityEvent _onWalkingStart;
    [SerializeField] private UnityEvent _onRunningStart;
    public bool IsDashing => _dashTimer > 0.0f;
    public bool IsRunning => !IsWalking && !IsDashing;

    private float _currentSpeed;
    public float CurrentSpeed => _currentSpeed;

    //Dash
    private bool _isDashReady = true;

    private float _dashCooldownTimer;
    public float DashCooldownTimer => _dashCooldownTimer;

    public bool ForceWalk => _forceWalk;

    private PlayerStats _stats;
    private Animator _animator;

    public UnityEvent dash_Event;

    private void Awake()
    {
        _stats = GetComponent<PlayerStats>();
        if (_stats == null)
        {
            Debug.LogWarning(name + " has a Controller but no PlayerStats. Destroying the Controller.");
            Destroy(this);
            return;
        }

        _animator = GetComponent<Animator>();
        if (_animator == null)
        {
            Debug.LogWarning(name + " has a Controller but no Animator. Destroying the Controller.");
            Destroy(this);
            return;
        }

        if (dash_Event == null)
            dash_Event = new UnityEvent();

        _dashCooldownTimer = _stats.DashCooldown;
    }

    private void Update()
    {
        if (_stats.isDead) return;
        UpdateTimers();
        UpdateInput();

        if (Stopped) return;

        _currentSpeed = _stats.RunSpeed;
        if (IsWalking) _currentSpeed = _stats.WalkSpeed;
        if (_dashTimer > 0.0f)
        {
            _currentSpeed = _stats.DashSpeed;
        }
        else
        {
            _animator.SetBool("isDashing", false);
        }

        transform.position += Time.deltaTime * _currentSpeed * Vector3.right;
    }

    private void UpdateTimers()
    {
#if UNITY_ANDROID
        _timeSinceLastTouchEnded += Time.deltaTime;
        _timeSinceLastTouchBegan += Time.deltaTime;

#endif
        if (_dashTimer > 0.0f) _dashTimer -= Time.deltaTime;
        if (_stopTimer > 0.0f) _stopTimer -= Time.deltaTime;
        else Stopped = false;

        if (!_isDashReady) _dashCooldownTimer += Time.deltaTime;
        if (_dashCooldownTimer >= _stats.DashCooldown)
        {
            _isDashReady = true;
        }
    }

    private void UpdateInput()
    {
#if UNITY_ANDROID
#if UNITY_EDITOR
        if (Input.GetButtonDown("Fire1"))
        {
            _timeSinceLastTouchBegan = 0.0f;
        }
        if (Input.GetButtonUp("Fire1"))
        {
            if (_timeSinceLastTouchBegan <= _tapTime && _timeSinceLastTouchEnded <= _doubleTapTime && _isDashReady)
                Dash();

            _timeSinceLastTouchEnded = 0.0f;
            IsWalking = false;
        }
#endif
        if (Input.touchCount > 0)
        {
            var touch = Input.GetTouch(0);

            switch (touch.phase)
            {
                case TouchPhase.Began:
                    _timeSinceLastTouchBegan = 0.0f;
                    break;

                case TouchPhase.Ended:
                    if (_timeSinceLastTouchBegan <= _tapTime && _timeSinceLastTouchEnded <= _doubleTapTime && _isDashReady)
                        Dash();
                    _timeSinceLastTouchEnded = 0.0f;
                    IsWalking = false;
                    break;

                case TouchPhase.Stationary:
                case TouchPhase.Moved:
                case TouchPhase.Canceled:
                default:
                    break;
            }
        }
#if !UNITY_EDITOR
        else IsWalking = false;
#endif
        if (_timeSinceLastTouchBegan > _tapTime && _timeSinceLastTouchBegan < _timeSinceLastTouchEnded)
            IsWalking = true;
#else
        IsWalking = Input.GetKey(_walkKey) || ForceWalk;

        if (Input.GetKeyDown(_dashKey) && _isDashReady)
            Dash();
#endif
        CheckForSpeedChange();
        _animator.SetBool("walking", IsWalking);
    }

    private void CheckForSpeedChange()
    {
        if (IsWalking && !_wasWalkingLastFrame && !ForceWalk)
        {
            _onWalkingStart.Invoke();
            _wasWalkingLastFrame = true;
        }

        if (!IsWalking && _wasWalkingLastFrame)
        {
            _onRunningStart.Invoke();
            _wasWalkingLastFrame = false;
        }
    }

    private bool _forceWalk;
    public void SetForceWalk(bool status)
    {
        _forceWalk = status;
    }

    private void Dash()
    {
        _stats.IncreaseHealthDash();
        _isDashReady = false;
        dash_Event.Invoke();
        _dashTimer = _stats.DashDuration;
        _dashCooldownTimer = 0f;
        _animator.SetBool("isDashing", true);
    }
}
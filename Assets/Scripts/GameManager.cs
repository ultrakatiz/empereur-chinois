﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[Serializable]
public class ObstacleCollisionEvent : UnityEvent<Obstacle> {}

public class GameManager : MonoBehaviour
{
    public readonly UnityEvent WinEvent = new UnityEvent();

    private bool _gameOver;

    [SerializeField]
    private int scoreIncrementPerSecond;
    public int ScoreIncrementPerSecond
    {
        get { return scoreIncrementPerSecond; }
        set { scoreIncrementPerSecond = value; }
    }

    private float timeSinceLastIncrement;
    public static GameManager Instance { get; private set; }
    
    public Player Player { get; private set; }
    public readonly ObstacleCollisionEvent ObstacleCollisionEvent = new ObstacleCollisionEvent();

    [SerializeField]
    private int score;
    public int Score
    {
        get { return score; }
        set { score = value; }
    }

    [SerializeField]
    private ZoneStats currentZone;
    public ZoneStats CurrentZone
    {
        get { return currentZone; }
        set { currentZone = value; }
    }

    private float currentZoneTimer = 0f;

    private bool shameActionDone = false;

    private Controller _controller;

    private PlayerStats _playerStats;

    public void Awake()
    {
        if (Instance != null && Instance != this)
        {
            Debug.LogWarning("A GameManager already exists. Deleting " + name + "'s GameManager.");
            Destroy(this);
            return;
        }
        Instance = this;

        var player = GameObject.FindWithTag("Player");
        if (player == null) Debug.LogWarning("There is no GameObject tagged with Player in the scene !");
        else
        {
            Player = player.GetComponent<Player>();
            if (Player == null) Debug.LogWarning("Player does not have a Player component.");
        }

        if (_controller == null)
        {
            _controller = player.GetComponent<Controller>();
            if(_controller == null)
            {
                Debug.LogWarning("Player doesn't have Controller");
                Destroy(this);
                return;
            }
        }

        if (_playerStats == null)
        {
            _playerStats = player.GetComponent<PlayerStats>();
            if (_playerStats == null)
            {
                Debug.LogWarning("Player doesn't have PlayerStats");
                Destroy(this);
                return;
            }
        }
        _controller.dash_Event.AddListener(DashInZone);
        currentZoneTimer = 0f;
    }

    private void Start()
    {
        WinEvent.AddListener(OnWin);
    }

    public void Update()
    {
        if (_gameOver) return;
        if (Player.Stats.isDead)
        {
            _gameOver = true;
            return;
        }

        if (CurrentZone != null)
        {
            float runTime = 0;
            if (CurrentZone.type == ZoneStats.ZoneType.CrowdedLook)
            {
                if (_controller.IsRunning) TickDecreaseScore();
                if (_controller.IsWalking && currentZone.CanChange) runTime = Time.deltaTime;
                currentZone.CheckChange(runTime, shameActionDone, false);
            }
            else
            {
                if (currentZoneTimer != 0) currentZoneTimer = 0;
                if (currentZone.CanChange)
                {
                    if (_controller.IsRunning) runTime = Time.deltaTime;
                    currentZone.CheckChange(runTime, shameActionDone,true);
                }
            }
        }
        shameActionDone = false;
        timeSinceLastIncrement += Time.deltaTime;
        if (timeSinceLastIncrement >= 1)
        {
            timeSinceLastIncrement = 0;
            score += scoreIncrementPerSecond;
        }
    }

    private void DashInZone()
    {
        if (CurrentZone == null) return;
        shameActionDone = true;
        switch (CurrentZone.type)
        {
            case ZoneStats.ZoneType.Alone:
                break;
            case ZoneStats.ZoneType.Crowded:
                Score -= CurrentZone.DashDamage;
                break;
            case ZoneStats.ZoneType.CrowdedLook:
                Score -= CurrentZone.DashDamage;
                break;
        }
    }

    private void TickDecreaseScore()
    {
        currentZoneTimer += Time.deltaTime;
        if (currentZoneTimer >= CurrentZone.DecreaseScoreFrequency)
        {
            currentZoneTimer = 0f;
            score -= CurrentZone.DecreaseScoreRate;
        }
    }

    private void OnWin()
    {
        _gameOver = true;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(Renderer))]
public class MovingBackground : MonoBehaviour
{
    private Renderer _renderer;

    public bool StopScrolling;

    private void Awake()
    {
        _renderer = GetComponent<Renderer>();
        if (_renderer == null)
        {
            Debug.LogWarning(name + " has a MovingBackground component but no Renderer. Destroying MovingBackground.");
            Destroy(this);
            return;
        }
    }

    private void Start()
    {
        GameManager.Instance.Player.Stats.BeforeDeath.AddListener(StopWithDeath);
    }

    private void Update()
    {
        if (StopScrolling) return;

        _renderer.material.mainTextureOffset +=
            Time.deltaTime * GameManager.Instance.Player.Controller.CurrentSpeed / transform.localScale.x * Vector2.right;
    }

    private void StopWithDeath()
    {
        StopScrolling = true;
    }
}

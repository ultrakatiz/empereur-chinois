﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ZoneStats : MonoBehaviour
{
    public enum ZoneType {Crowded, Alone, CrowdedLook };

    public ZoneType type;

    [SerializeField]
    private BoxCollider2D _collider;

    private GameManager _gameManager;

    //If CrowdedLook
    [SerializeField]
    private int decreaseScoreRate;
    public int DecreaseScoreRate
    {
        get { return decreaseScoreRate; }
        set { decreaseScoreRate = value; }
    }
    [SerializeField]
    private float decreaseScoreFrequency;
    public float DecreaseScoreFrequency
    {
        get { return decreaseScoreFrequency; }
        set { decreaseScoreFrequency = value; }
    }

    [SerializeField]
    private bool canChange = false;
    public bool CanChange
    {
        get { return canChange; }
        set { canChange = value; }
    }

    [SerializeField]
    private bool isChanging = false;
    public bool IsChanging
    {
        get { return isChanging; }
        set { isChanging = value; }
    }

    [SerializeField]
    private int dashDamage;
    public int DashDamage
    {
        get { return dashDamage; }
        set { dashDamage = value; }
    }

    [SerializeField]
    private float maxRunTime;

    [SerializeField]
    private float runCount;

    [SerializeField]
    private float maxGoodBehavior;

    [SerializeField]
    private float goodBehaviorCount;

    private void Awake()
    {
        if(_collider == null)
        {
            _collider = GetComponent<BoxCollider2D>();
            if(_collider == null)
            {
                Debug.LogWarning(name + " has a ZoneStats but no Collider2D. Destroying the ZoneStats.");
                Destroy(this);
                return;
            }
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            GameManager.Instance.CurrentZone = this;
        }
    }

    public void CheckChange(float t, bool shameAction, bool isRunning)
    {
        if (shameAction) goodBehaviorCount = 0f;
        if (isRunning)
        {
            goodBehaviorCount = 0f;
            runCount += t;
            if (runCount >= maxRunTime || shameAction)
            {
                runCount = 0f;
                CanChange = false;
                IsChanging = true;
            }
        } else
        {
            if (maxGoodBehavior == 0) return;
            goodBehaviorCount += t;
            if (goodBehaviorCount >= maxGoodBehavior)
            {
                goodBehaviorCount = 0f;
                CanChange = false;
                IsChanging = true;
            }
        }

    }

}

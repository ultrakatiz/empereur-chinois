﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class FallingObstacle : Obstacle
{
    [SerializeField] private float _fallDistance;
    [SerializeField] private float _fallSpeed;

    [SerializeField] private Sprite brokenObject;
    
    private float _fallDistanceCounter;
    private bool _isFalling;
    private SpriteRenderer _renderer;
    private Collider2D _collider2D;
    private AudioSource _audioSource;
    
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
        _collider2D = GetComponent<Collider2D>();
        _renderer = GetComponent<SpriteRenderer>();
    }

    protected override void OnPlayerCollision() {}

    public void Fall()
    {
        _isFalling = true;
    }

    private void Update()
    {
        if (!_isFalling) return;

        if (_fallDistanceCounter >= _fallDistance)
        {
            _isFalling = false;
            _renderer.sprite = brokenObject;
            _collider2D.enabled = false;
            _audioSource.Play();
            return;
        }

        transform.position -= Time.deltaTime * _fallSpeed * Vector3.up;
        _fallDistanceCounter += Time.deltaTime * _fallSpeed;
    }
}

﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WalkingNobleObstacle : Obstacle
{
    [SerializeField] private List<AudioClip> _clips;
    private AudioSource _audioSource;
    private float _waitUntilNextClipTime = 1.0f;
    private float _waitUntilNextClipTimer;

    [SerializeField] private float _speed = 0.5f;

    [SerializeField] private int _scorePerTick = 1;
    [SerializeField] private float _addScorePeriod = 0.5f;
    private float _addScoreTimer;

    private bool _walkingWithPlayer;

    private Animator _animator;

    private bool _stopMoving = false;

    private GameObject _bubulObject;

    private new void Awake()
    {
        _animator = GetComponent<Animator>();
        if (_animator == null)
        {
            Debug.LogWarning(name + " has a WalkingNobleObstacle but no Animtor. Destroying the WalkingNobleObstacle.");
            Destroy(this);
            return;
        }
        _bubulObject = this.transform.GetChild(0).gameObject;
        if (_bubulObject)
        {
            Debug.LogWarning(name + " has a WalkingNobleObstacle but no Child.");
        }
        _bubulObject.SetActive(false);

        _audioSource = GetComponent<AudioSource>();
        if (_audioSource == null) Debug.LogWarning(name + " has no AudioSource.");
    }

    protected override void OnPlayerCollision()
    {
        _walkingWithPlayer = true;
    }

    private void Update()
    {
        var player = GameManager.Instance.Player;
        if (_walkingWithPlayer && player.Controller.IsWalking)
        {
            _animator.SetBool("Talking", true);
            transform.position += Time.deltaTime * player.Stats.WalkSpeed * Vector3.right;

            if (!_audioSource.isPlaying)
            {
                _waitUntilNextClipTimer -= Time.deltaTime;
                if (_waitUntilNextClipTimer <= 0.0f)
                {
                    _waitUntilNextClipTimer = _waitUntilNextClipTime;
                    _audioSource?.PlayOneShot(_clips[Random.Range(0, _clips.Count - 1)]);
                }
            }

            _addScoreTimer -= Time.deltaTime;
            if (_addScoreTimer > 0.0f) return;

            _bubulObject.SetActive(true);
            _bubulObject.transform.localScale = new Vector3(Mathf.PingPong(Time.time,0.5f)+0.5f, Mathf.PingPong(Time.time, 0.5f)+0.5f,0);
            _addScoreTimer += _addScorePeriod;
            GameManager.Instance.Score += _scorePerTick;
            return;
        } else if (_walkingWithPlayer)
        {
            _bubulObject.SetActive(true);
            _animator.SetBool("Talking", true);
        }
        _walkingWithPlayer = false;
        if (_stopMoving)
        {
            _bubulObject.SetActive(false);
            _animator.SetBool("Stop", true);
            return;
        }
        transform.position += Time.deltaTime * _speed * Vector3.left;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
        {
            _stopMoving = true;
        }
    }
}
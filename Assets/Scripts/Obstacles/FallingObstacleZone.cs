﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FallingObstacleZone : MonoBehaviour
{
    private FallingObstacle _obstacle;

    private void Awake()
    {
        _obstacle = GetComponentInChildren<FallingObstacle>();
        if (_obstacle == null)
        {
            Debug.LogWarning(name + " has a FallingObstacleZone component but no FallingObstacle child. Destroying FallingObstacleZone.");
            Destroy(this);
            return;
        }
    }

    private void OnTriggerEnter2D(Collider2D other)
    {
        if (!other.CompareTag("Player")) return;

        _obstacle.Fall();
    }
}

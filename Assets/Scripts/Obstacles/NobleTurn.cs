﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(ZoneStats))]
public class NobleTurn : Obstacle
{
    [SerializeField] private List<AudioClip> _clips;

    enum NobleState {Waiting,Looking,Judging }

    [SerializeField]
    private NobleState State;

    [SerializeField]
    private bool canTurn = false;

    [SerializeField]
    private Sprite WaitingSprite;

    [SerializeField]
    private Sprite LookingSprite;

    [SerializeField]
    private Sprite JudgeSprite;

    private SpriteRenderer _spriteRenderer;

    private ZoneStats _zoneNoble;

    private AudioSource _audioSource;

    private new void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        if(_spriteRenderer == null)
        {
            Debug.LogWarning(name + " has a NobleTurn but no SpriteRenderer. Destroying the NobleTurn.");
            Destroy(this);
            return;
        }

        _zoneNoble = GetComponent<ZoneStats>();
        if (_zoneNoble == null)
        {
            Debug.LogWarning(name + " has a NobleTurn but no ZoneStats. Destroying the NobleTurn.");
            Destroy(this);
            return;
        }
        _zoneNoble.CanChange = canTurn;

        switch (State)
        {
            case (NobleState.Waiting):
                TurnBack();
                break;
            case (NobleState.Looking):
                TurnLook();
                break;
            case (NobleState.Judging):
                TurnJudge();
                break;
        }

        _audioSource = GetComponent<AudioSource>();
        if (_audioSource == null) Debug.LogWarning(name + " has no AudioSource.");
    }

    private void Update()
    {
        if (!canTurn) return;

        if (_zoneNoble.IsChanging)
        {
            _zoneNoble.CanChange = true;
            _zoneNoble.IsChanging = false;
            if (State == NobleState.Looking) TurnJudge();
            else if (State == NobleState.Judging) TurnLook();

        }
    }

    protected override void OnPlayerCollision()
    {
        if (!canTurn) return;
        TurnLook();

    }

    private void TurnBack()
    {
        _zoneNoble.type = ZoneStats.ZoneType.Alone;
        _spriteRenderer.sprite = WaitingSprite;
        State = NobleState.Waiting;
    }

    private void TurnLook()
    {
        _zoneNoble.type = ZoneStats.ZoneType.Crowded;
        _spriteRenderer.sprite = LookingSprite;
        State = NobleState.Looking;
    }

    private void TurnJudge()
    {
        _audioSource?.PlayOneShot(_clips[Random.Range(0, _clips.Count - 1)]);
        _zoneNoble.type = ZoneStats.ZoneType.CrowdedLook;
        _spriteRenderer.sprite = JudgeSprite;
        State = NobleState.Judging;
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        if(collision.CompareTag("Player"))
            TurnBack();
    }



}

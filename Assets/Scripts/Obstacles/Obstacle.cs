﻿using System;
using UnityEngine;
using UnityEngine.Events;

[RequireComponent(typeof(Rigidbody2D), typeof(Collider2D))]
public abstract class Obstacle : MonoBehaviour
{
    [SerializeField] protected int _damage = 3;
    [SerializeField] protected int _scoreDamage;
    public int Damage => _damage;
    public int ScoreDamage => _scoreDamage;

    private bool hasTouchedPlayer;

    [SerializeField] private bool _ignorePlayerWalking;
    [SerializeField] private bool _ignorePlayerRunning;
    [SerializeField] private bool _ignorePlayerDashing;

    protected Rigidbody2D _rigidbody;
    protected Collider2D _collider;

    protected void Awake()
    {
        _rigidbody = GetComponent<Rigidbody2D>();
        if (_rigidbody == null)
        {
            Debug.LogWarning(name + " has a Controller but no Rigidbody2D. Destroying the Controller.");
            Destroy(this);
            return;
        }
        _rigidbody.bodyType = RigidbodyType2D.Kinematic;

        _collider = GetComponent<Collider2D>();
        if (_collider == null)
        {
            Debug.LogWarning(name + " has a Controller but no Collider2D. Destroying the Controller.");
            Destroy(this);
            return;
        }
    }

    private void OnTriggerStay2D(Collider2D other)
    {
        if (hasTouchedPlayer || !other.CompareTag("Player")) return;

        var controller = GameManager.Instance.Player.Controller;
        if (controller.IsWalking && _ignorePlayerWalking) return;
        if (controller.IsRunning && _ignorePlayerRunning) return;
        if (controller.IsDashing && _ignorePlayerDashing) return;

        hasTouchedPlayer = true;
        GameManager.Instance.ObstacleCollisionEvent.Invoke(this);
        OnPlayerCollision();
    }

    protected abstract void OnPlayerCollision();
}

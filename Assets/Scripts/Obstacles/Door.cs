﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[RequireComponent(typeof(AudioSource))]
public class Door : MonoBehaviour
{
    private Collider2D _collider;

    [SerializeField]
    private GameObject noblePrefab;

    [SerializeField]
    private bool canOpen;

    [SerializeField]
    private bool spawnNoble;

    private bool isOpen = false;
    private bool startOpen = false;
    private float openTimer;

    [SerializeField]
    private float durationBeforeOpen;

    [SerializeField]
    private Sprite OpenDoor;

    private AudioSource _audioSource;
    //Test
    Vector2 startingRot;
    public float speed, amount;
    [SerializeField] private AudioClip shakingSound;
    [SerializeField] private AudioClip openingSound;

    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Awake()
    {
        startingRot.x = transform.rotation.y;
        startingRot.y = transform.rotation.z;
    }

    private void Update()
    {
        if(startOpen && !isOpen)
        {
            openTimer += Time.deltaTime;
            if(openTimer >= durationBeforeOpen)
            {
                isOpen = true;
                startOpen = false;
                transform.rotation = Quaternion.Euler(Vector3.zero);
                _audioSource.PlayOneShot(openingSound);
                ChangeSprite();
                if (spawnNoble) InitNoble();
            }
        }

        if(startOpen)
        {
            ShakeDoor();
        }
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.CompareTag("Player") && !isOpen)
        {
            startOpen = true;
            openTimer = 0f;
            if(!_audioSource.isPlaying)
                _audioSource.PlayOneShot(shakingSound);
        }
    }

    private void ChangeSprite()
    {
        GetComponent<SpriteRenderer>().sprite = OpenDoor;
        transform.position += 0.18f * Vector3.down;
    }

    private void InitNoble()
    {
        Instantiate(noblePrefab, this.transform.position, Quaternion.identity);
    }

    private void ShakeDoor()
    {
        transform.rotation = Quaternion.Euler(0f,startingRot.x + Mathf.Sin(Time.time * speed) * amount ,startingRot.y + (Mathf.Sin(Time.time * speed) * amount));
    }

}

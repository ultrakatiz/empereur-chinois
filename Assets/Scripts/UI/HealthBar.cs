﻿using UnityEngine;
using UnityEngine.UI;

public class HealthBar : MonoBehaviour
{
    private const float _minFill = 0.3f;
    private const float _maxFill = 0.8f;

    private Image _image;
    private Color _originalColor;
    private float _colorChange;
    [SerializeField] private float _colorChangeSpeed = 1.0f;

    private void Awake()
    {
        _image = GetComponent<Image>();
        if (_image == null)
        {
            Debug.LogWarning(name + " has a HealthBar component but no Image. Destroying HealthBar.");
            Destroy(this);
            return;
        }
        _image.type = Image.Type.Filled;
        _image.fillMethod = Image.FillMethod.Vertical;
        _image.fillOrigin = 0;

        _originalColor = _image.color;
    }

    private void Start()
    {
        GameManager.Instance.ObstacleCollisionEvent.AddListener(OnObstacleCollision);
    }

    private void Update()
    {
        var stats = GameManager.Instance.Player.Stats;
        var healthRatio = (float) stats.CurrentHealthPoints / stats.MaxHealthPoints;
        _image.fillAmount = _minFill + healthRatio * (_maxFill - _minFill);

        if (_colorChange > 0.0f) _colorChange -= Time.deltaTime * _colorChangeSpeed;
        else _colorChange = 0.0f;

        _image.color = _colorChange * Color.red + (1.0f - _colorChange) * _originalColor;
    }

    private void OnObstacleCollision(Obstacle obstacle)
    {
        if (obstacle.Damage <= 0) return;

        _colorChange = 1.0f;
        _image.color = Color.red - _originalColor;
    }
}

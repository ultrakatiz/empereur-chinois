﻿using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ScoreChange : MonoBehaviour
{
    [SerializeField] private float _travelTime = 1.0f;
    [SerializeField] private float _travelSpeed = 1.0f;

    private TextMeshProUGUI _text;

    public int ChangeAmount
    {
        set
        {
            if (value >= 0)
            {
                _text.color = Color.green;
                _text.text = "+" + value;
            }
            else
            {
                _text.color = Color.red;
                _text.text = value.ToString();
            }
        }
    }

    private float _timer;

    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
        if (_text == null)
        {
            Debug.LogWarning(name + " has a ScoreChange component but no TextMeshProUGUI. Destroying ScoreChange.");
            Destroy(this);
            return;
        }
    }

    private void Update()
    {
        _timer += Time.deltaTime;

        if (_timer > _travelTime)
        {
            Destroy(gameObject);
            return;
        }

        transform.localPosition += Time.deltaTime * _travelSpeed * Vector3.up;
        _text.color = new Color(_text.color.r, _text.color.g, _text.color.b, 1.0f - _timer / _travelTime);
    }
}
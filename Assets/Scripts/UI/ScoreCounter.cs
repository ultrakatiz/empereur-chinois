﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class ScoreCounter : MonoBehaviour
{
    [SerializeField] private String _preText = "SCORE : ";
    [SerializeField] private ScoreChange _scoreChangePrefab;

    private int _score;
    private TextMeshProUGUI _text;

    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
        if (_text == null)
        {
            Debug.LogWarning(name + " has a ScoreCounter component but no TextMeshProUGUI. Destroying ScoreCounter.");
            Destroy(this);
            return;
        }
    }

    void Update()
    {
        if (_score != GameManager.Instance.Score)
        {
            var scoreChange = Instantiate(_scoreChangePrefab, transform);
            scoreChange.ChangeAmount = GameManager.Instance.Score - _score;
            _score = GameManager.Instance.Score;
        }

        _text.text = _preText + _score;
    }
}

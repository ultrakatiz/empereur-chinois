﻿using UnityEngine;
using UnityEngine.UI;

public class DashBar : MonoBehaviour
{
    private const float _minFill = 0.0f;
    private const float _maxFill = 0.875f;

    private Image _image;

    private void Awake()
    {
        _image = GetComponent<Image>();
        if (_image == null)
        {
            Debug.LogWarning(name + " has a DashBar component but no Image. Destroying HealthBar.");
            Destroy(this);
            return;
        }
        _image.type = Image.Type.Filled;
        _image.fillMethod = Image.FillMethod.Radial360;
        _image.fillOrigin = 0;
    }

    private void Update()
    {
        var player = GameManager.Instance.Player;
        var dashRatio = player.Controller.DashCooldownTimer / player.Stats.DashCooldown;
        _image.fillAmount = _minFill + dashRatio * (_maxFill - _minFill);
    }
}

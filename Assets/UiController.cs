﻿using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UiController : MonoBehaviour
{
    [SerializeField] private Image panel;
    [SerializeField] private Image illustration;
    [SerializeField] private Sprite WinScreen;
    [SerializeField] private Sprite LoseScreen;
    [SerializeField] private Button NextLevelButton;
    public void DisplayWinScreen()
    {
        illustration.sprite = WinScreen; 
        panel.gameObject.SetActive(true);
        NextLevelButton.gameObject.SetActive(true);
        NextLevelButton.onClick.RemoveAllListeners();
        NextLevelButton.onClick.AddListener(delegate
        {
            SceneManager.LoadScene(LevelTracker.Instance.GetNextLevelAndIncrement());
        });
    }

    public void DisplayLoseScreen()
    {
        illustration.sprite = LoseScreen;
        panel.gameObject.SetActive(true);
        NextLevelButton.gameObject.SetActive(false);
    }
}

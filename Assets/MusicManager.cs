﻿using System;
using TMPro;
using UnityEngine;
using UnityEngine.Experimental.PlayerLoop;
using Random = UnityEngine.Random;

public class MusicManager : MonoBehaviour
{
    [SerializeField] private AudioClip _runningMusic;
    [SerializeField] private AudioClip _shameMusic;
    [SerializeField] private AudioClip[] _walkingMusics;

    private AudioSource _audioSource;

    private bool shameIsPlaying;

    // Start is called before the first frame update
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        if (_audioSource.clip != _shameMusic)
            shameIsPlaying = false;
    }

    public void PlayRunningMusic()
    {
        _audioSource.Stop();
        _audioSource.PlayOneShot(_runningMusic);
    }

    public void PlayRandomWalkingMusic()
    {
        if (shameIsPlaying) return;
        _audioSource.Stop();
        var randomIndex = Random.Range(0, _walkingMusics.Length);
        _audioSource.PlayOneShot(_walkingMusics[randomIndex]);
    }

    public void PlayShameMusic()
    {
        shameIsPlaying = true;
        _audioSource.Stop();
        _audioSource.PlayOneShot(_shameMusic);
    }
}

﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Random = UnityEngine.Random;

[RequireComponent(typeof(AudioSource))]
public class SfxManager : MonoBehaviour
{
    private AudioSource _audioSource;
    [SerializeField] private AudioClip hurtSound;
    [SerializeField] private AudioClip dashSound;
    [SerializeField] private float randomPitchDelta;
    // Start is called before the first frame update
    private void Start()
    {
        _audioSource = GetComponent<AudioSource>();
    }

    private void Update()
    {
        // return to base pitch if we are done playing sound effect
        if (!_audioSource.isPlaying)
            _audioSource.pitch = 1;
    }

    public void PlayHurtSound()
    {
        _audioSource.pitch = getRandomPitchInDelta(randomPitchDelta);
        _audioSource.PlayOneShot(hurtSound);
    }

    public void PlayDashSound()
    {
        _audioSource.pitch = getRandomPitchInDelta(randomPitchDelta);
        _audioSource.PlayOneShot(dashSound);
    }
    
    public float getRandomPitchInDelta(float delta)
    {
        float min = 1 - delta;
        float max = 1 + delta;
        return Random.Range(min, max);
    }
}

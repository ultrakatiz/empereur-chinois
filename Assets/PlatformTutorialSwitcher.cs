﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlatformTutorialSwitcher : MonoBehaviour
{
    [SerializeField] private Image phonePart;

    [SerializeField] private Image pcPart;
    // Start is called before the first frame update
    void Start()
    {
        phonePart.gameObject.SetActive(false);
        pcPart.gameObject.SetActive(true);
#if UNITY_ANDROID
            phonePart.gameObject.SetActive(true);
            pcPart.gameObject.SetActive(false);
#endif

    }
}

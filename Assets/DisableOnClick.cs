﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DisableOnClick : MonoBehaviour
{
    [SerializeField] private Image toDisable;
    public void Disable()
    {
        toDisable.gameObject.SetActive(false);
    }
}

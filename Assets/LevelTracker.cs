﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LevelTracker : MonoBehaviour
{
    public static LevelTracker Instance;
    
    [SerializeField] private int _startLevel;
    [SerializeField] private int _maxLevel;

    private int _currentLevel;

    public void Awake()
    {
        _currentLevel = _startLevel;
        if (Instance == null)
            Instance = this;
        if (Instance != this)
        {
            Debug.Log("There is already a LevelTracker instance in the scene. Destroying !");
            Destroy(this);
        }
    }

    public int GetNextLevelAndIncrement()
    {
        _currentLevel++;
        if (_currentLevel > _maxLevel)
        {
            _currentLevel = _startLevel;
            return 0;
        }
        return _currentLevel;
    }
}
